#include "list.h"
using namespace std;

/*
	This main.cpp file will serve as a test bed for the user to call
	my functions. Using the menu to call the functions in multiple different orders.
	If the results of the functions are 0 then it should output an error message.
*/

int main()
{
	stack my_stack;		// object for stack
	queue my_queue;		// object for queue
	binder to_add;		// object for user to input binder info
	binder to_peek;		// this will hold the peek info
	to_add.subject = new char[SIZE];	// allocating the dynamic arrays if the binder items
	to_add.status = new char[SIZE];
	to_add.description = new char[SIZE];
	to_add.due_date = new char[SIZE];
	todo user_todo;		// object for user to input todo info
	todo * todo_peek;	// this will hold the todo peek info
	int choice;	// users menu choice
	int result;	// error checker

	do
	{
		cout << "\nMENU: " << endl
			<< "[1] ADD BINDER ITEM" << endl 
			<< "[2] REMOVE RECENT BINDER ITEM" << endl
			<< "[3] PREVIEW RECENT BINDER ITEM" << endl
			<< "[4] DISPLAY BINDER ITEMS" << endl
			<< "[5] ADD A TODO ITEM TO BINDER" << endl
			<< "[6] REMOVE OLDEST TODO ITEM" << endl
			<< "[7] DISPLAY TODO ITEMS" << endl
			<< "[8] PREVIEW NEXT TODO ITEM" << endl
			<< "[9] EXIT" << endl << endl; 
		// get user menu choice
		cout << "Enter menu choice: ";
		cin >> choice;
		while(!cin || cin.peek() != '\n')       // validate the user enters number not a char
		{
			cin.clear();
			cin.ignore(100, '\n');
			cout << "Invalid Number. Try again." << endl
		 	     << "Enter your choice: ";
			cin >> choice;
		}
		cin.get();	
		if(choice == 1)
		{
			cout << "Enter in a Subject: ";
			cin.get(to_add.subject, SIZE, '\n');
			cin.ignore(100, '\n');
			cout << "Enter in Status: ";
			cin.get(to_add.status, SIZE, '\n');
			cin.ignore(100, '\n');
			cout << "Enter in Description: ";
			cin.get(to_add.description, SIZE, '\n');
			cin.ignore(100, '\n');
			cout << "Enter in Due Date: ";
			cin.get(to_add.due_date, SIZE, '\n');
			cin.ignore(100, '\n');
			result = my_stack.push(to_add, my_queue);
			if(result == 0)
				cout << "FAILED TO ADD" << endl;
			else if(result == 1)
				cout << "ADDED SUCCESSFULLY" << endl;
		}
		if(choice == 2)	
		{
			result = my_stack.pop();
			if(result == 0)
				cout << "FAILED TO REMOVE" << endl;
			else if(result == 1)
				cout << "REMOVED SUCCESSFULLY" << endl;
		}
		if(choice == 3)
		{
			result = my_stack.peek(to_peek);
			if(result == 1)
			{
				cout << "\nTOP BINDER ITEM:" << endl
				     << "Subject: " << to_add.subject << endl
				     << "Status: " << to_add.status << endl
				     << "Description: " << to_add.description << endl
				     << "Due Date: " << to_add.due_date << endl;
			}
			else if(result == 0)
				cout << "Nothing in binder" << endl;
		}
		if(choice == 4)
		{
			result = my_stack.display();
			if(result == 0)
				cout << "Nothing to display" << endl;
		}
		if(choice == 5)
		{	
			user_todo.name = new char[SIZE];
			user_todo.description = new char[SIZE];
			user_todo.link = new char[100];
			cout << "Enter todo task: ";
			cin.get(user_todo.name, SIZE, '\n');
			cin.ignore(100, '\n');
			cout << "Enter in Description of task: ";
			cin.get(user_todo.description, SIZE, '\n');
			cin.ignore(100, '\n');
			cout << "Enter in link/resource: ";
			cin.get(user_todo.link, 100, '\n');
			cin.ignore(100, '\n');
			cout << "Enter level of Priority of this task (1-5)" << endl
			     << "1 being highest priority and 5 the lowest: ";
			cin >> user_todo.level;
			while(!cin || cin.peek() != '\n' || user_todo.level < 1 || user_todo.level > 5)       // validate the user enters number not a char
			{
				cin.clear();
				cin.ignore(100, '\n');
				cout << "Invalid Number. Try again." << endl
				     << "Enter number 1-5: ";
				cin >> user_todo.level;
			}
			cin.get();
			result = my_queue.enqueue(user_todo);
			if(result == 0)
				cout << "FAILED TO ADD" << endl;
			else if(result == 1)
				cout << "ADDED SUCCESSFULLY" << endl;
		}
		if(choice == 6)
		{
			result = my_queue.dequeue();
			if(result == 0)
				cout << "There are no todos. Nothing to remove." << endl;
			else if(result == 1)
				cout << "REMOVED SUCCESSFULLY" << endl;
		}
		if(choice == 7)
		{
			result = my_queue.todo_display();
			if(result == 0)
				cout << "Nothing to display" << endl;
		}
		if(choice == 8)
		{
			result = my_queue.peek(todo_peek);
			if(result == 1)
			{	
				cout << "Top Todo Task" << endl
				     << "\nName: " << todo_peek->name << endl
				     << "Description: " << todo_peek->description << endl
				     << "Link: " << todo_peek->link << endl
				     << "Level of Priority: " << todo_peek->level << endl << endl;
			}
			else if(result ==  0)
				cout << "There are no ToDos" << endl;				
		}

		}while(choice != 9);
		cout << "\nThank You !" << endl;
		
		delete [] to_add.subject;
		delete [] to_add.status;
		delete [] to_add.description;
		delete [] to_add.due_date;
		
		return 0;
}
