#include <iostream>
#include <cstring>
#include <cctype>

/* Christine Nguyen, CS163, Program 2, 04/25/2023
   The purpose of this Program is to use stacks and queues
   to store todo items in a binder. There will be a class for
   stacks, queues, and another class to manipulate the 
   data structures.
*/


// max size of the array
const int MAX = 5;	// the max size for the array
const int SIZE = 50;	// the size for the information from user


// binder information
struct binder
{
	char * subject;
	char * status;
	char * description;
	char * due_date;
	class queue * ptr;	//pointer to queue
};


// node for binder
struct binder_node
{
	binder * a_binder;
	binder_node * next;
};


// todo information
struct todo
{
	char * name;
	char * description;
	char * link;
	int level;
};


// node for todo list
struct todo_node
{
	todo a_todo;
	todo_node * next;
};


// class for queue todo
class queue
{
	public:
		queue();	// default constructor
		~queue();	// destructor
		// to call these functions in main object.function();
		int enqueue(const todo &);	// function to add todo item
		int dequeue();		// function to remove first todo item
		int todo_display() const;	// function to display todo item
		int peek(todo *& todo_peek)const;	// function to peek first todo item
	private:
		todo_node * rear;	// node pointer to the rear of CLL
};	


// class for stuck serves as a binder
class stack
{
	public:
		stack();	// default constructor
                ~stack();	// destructor
		// to call these functions in main object.function();
                int peek(binder &) const;	// function to peek the latest binder item
                int push(const binder &, queue &);	// function to add a binder item
                int pop();	// function to remove the latest binder item
                int display();	// function calls recursive function
        private:
                binder_node * head;	// node pointer to head of LLL
                int top_index;	// index for the size of the array
                int binderNum;	// number of binder, not really needed but it counts the number of binders in list
                int display_r(binder_node * & head, int binderNum);	// recursive function
};
