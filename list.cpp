#include "list.h"
using namespace std;

/*
	This list.cpp file will implement the stack and queue functions.
	There will be the default constructor for both stack and queue which
	will initialize data members to nullptr or 0. The push function will
	serve as the add function to get the user info and strcpy to our lll
	or array. The pop function will remove the top node. The peek function
	will get the information from the top of the stack. The display will 
	display all of the information in the binder.
*/
	

// Constructor for stack
stack::stack()
{	
	head = nullptr;
	top_index = 0;
	binderNum = 0;
}


// Destructor for stack
stack::~stack()
{
	while(head)
	{
		binder_node * current = head;
		head = head->next;
		delete [] current->a_binder->subject;
		delete [] current->a_binder->status;
		delete [] current->a_binder->description;
		delete [] current->a_binder->due_date;
		delete current->a_binder;
		delete current;
	}
}


// Add a new binder item to the top of the stack
int stack::push(const binder & to_add, queue & a_queue)
{
	if(!head)
	{
		head = new binder_node;
		head->a_binder = new binder [MAX];
		head->next = nullptr;
	}
	if(top_index == MAX)
	{
		binder_node * current = new binder_node;
		current->a_binder = new binder [MAX];
		current->next = head;
		head = current;
		top_index = 0;
	}
	// copy data 
	head->a_binder[top_index].subject = new char[strlen(to_add.subject) + 1];	
	strcpy(head->a_binder[top_index].subject, to_add.subject);	
	head->a_binder[top_index].status = new char[strlen(to_add.status) + 1];
	strcpy(head->a_binder[top_index].status,  to_add.status);
	head->a_binder[top_index].description = new char[strlen(to_add.description) + 1];
	strcpy(head->a_binder[top_index].description, to_add.description);
	head->a_binder[top_index].due_date = new char[strlen(to_add.due_date) + 1];
	strcpy(head->a_binder[top_index].due_date, to_add.due_date);
	head->a_binder[top_index].ptr = &a_queue;
	++top_index;
	return 1;

}


// Remove the top of the stack
int stack::pop()
{
	if(!head)
		return 0;
	if(top_index == 0)
		return 0;
	if(top_index == 1)
	{
		binder_node * current = head;
		head = head->next;
		delete [] current->a_binder->subject;
		delete [] current->a_binder->status;
		delete [] current->a_binder->description;
		delete [] current->a_binder->due_date;
		delete current->a_binder;
		delete current;
		current = nullptr;
		if(head)
			top_index = MAX;
		else
			top_index = 0;
		return 1;
	}
	else
	{
		--top_index;
		delete [] head->a_binder[top_index].subject;
		delete [] head->a_binder[top_index].status;
		delete [] head->a_binder[top_index].description;
		delete [] head->a_binder[top_index].due_date;
		head->a_binder[top_index].subject = nullptr;
		head->a_binder[top_index].status = nullptr;
		head->a_binder[top_index].description = nullptr;
		head->a_binder[top_index].due_date = nullptr;
		return 1;
	}
	return 0;
	
}


// Peeks and gets the top item of the binder
int stack::peek(binder & to_peek) const
{
	if(!head)
		return 0;
	if(top_index > MAX)
		return 0;
	to_peek = *(head->a_binder);
	return 1;
}


// Displays all of the binder items
int stack::display()
{
	if(!head)
		return 0;
	head->a_binder->ptr->todo_display();
	return display_r(head, binderNum);
}


// Recursive funtion to display binder item and with implementation of Task #13 queue todos as well
int stack::display_r(binder_node * & head, int binderNum)
{
	if(!head)
		return 0;
	if(head->a_binder[0].subject)
		cout << "\nBINDER #" << ++binderNum << endl << endl;
	for(int i {0}; i < MAX; ++i)
	{
		if(head->a_binder[i].subject)	// i wanna make sure something is inside
		{ 
			cout << "Subject: " << head->a_binder[i].subject << endl
			     << "Status: " << head->a_binder[i].status << endl
			     << "Description: " << head->a_binder[i].description << endl
			     << "Due Date: " << head->a_binder[i].due_date << endl << endl;
		}
	}
	return 1 + display_r(head->next, binderNum);
}	


// Constructor of queue
queue::queue()
{
	rear = nullptr;
}

	
// Destructor of queue
queue::~queue()
{
	if(!rear)
		return;
	todo_node * current = rear;
	rear = rear->next;
	current->next = NULL;
	while(rear != NULL)
	{
		delete [] rear->a_todo.name;
		delete [] rear->a_todo.description;
		delete [] rear->a_todo.link;	
		current = rear->next;
		delete rear;
		rear = current;
	}
	rear = nullptr;
}


// Add todo (enqueue acts like a push function)
int queue::enqueue(const todo & to_add)
{
	todo_node * newNode = new todo_node;
	newNode->a_todo.name = new char[strlen(to_add.name) + 1];
	strcpy(newNode->a_todo.name, to_add.name);
	newNode->a_todo.description = new char[strlen(to_add.description) + 1];
	strcpy(newNode->a_todo.description, to_add.description);
	newNode->a_todo.link = new char[strlen(to_add.link) + 1];
	strcpy(newNode->a_todo.link, to_add.link);
	newNode->a_todo.level = to_add.level;
	
	if(!newNode)
		return 0;
	if(!rear)
	{
		rear = newNode;
		rear->next = rear;
	}
	else
	{
		newNode->next = rear->next;
		rear->next = newNode;
		rear = newNode;
	}
	return 1;
}


// Removes the lastes node being the rear of the CLL
int queue::dequeue()
{
	if(!rear)
		return 0;
	if(rear->next == rear)
	{
		delete rear;
		rear = NULL;
	}
	else
	{
		todo_node * temp = rear->next;
		rear->next = temp->next;
		delete [] temp->a_todo.name;
		delete [] temp->a_todo.description;
		delete [] temp->a_todo.link;
		delete temp;
	}
	return 1;
}


// Displays everything in the queue CLL
int queue::todo_display() const
{
	if(!rear)
		return 0;
	todo_node * current = rear->next;
	do
	{
		cout << "\nTODO ITEM" << endl
		     << "\nName: " << current->a_todo.name << endl
		     << "Description: " << current->a_todo.description << endl
		     << "Link: " << current->a_todo.link << endl
		     << "Level of Priority: " << current->a_todo.level << endl << endl;
		current = current->next;
	}while(current != rear->next);
	return 1;	
}


// Gives user the first (rear) infomation
int queue::peek(todo *& todo_peek)const
{
	if(!this->rear)
		return 0;
	//todo_peek = &(this->rear->next->a_todo);
	todo_peek = new todo;
    	todo_peek->name = new char[strlen(this->rear->next->a_todo.name) + 1];
    	strcpy(todo_peek->name, this->rear->next->a_todo.name);
    	todo_peek->description = new char[strlen(this->rear->next->a_todo.description) + 1];
	strcpy(todo_peek->description, this->rear->next->a_todo.description);
    	todo_peek->link = new char[strlen(this->rear->next->a_todo.link) + 1];
    	strcpy(todo_peek->link, this->rear->next->a_todo.link);
    	todo_peek->level = this->rear->next->a_todo.level;	
	return 1;
}

